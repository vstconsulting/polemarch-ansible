from __future__ import absolute_import, division, print_function
__metaclass__ = type

ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['stableinterface'],
                    'supported_by': 'core'}

DOCUMENTATION = '''
---
module: test_packed_module
short_description: Test module
description:
  - Test module for check functionality.
version_added: "0.0.2"
options:
  name:
    description:
      - Test description.
author: "Sergey Klyuykov"
notes:
   - Test module
'''
