import io
import os
import sys
import json
import unittest
try:
    from yaml import CLoader as Loader, load
except ImportError:  # nocv
    from yaml import Loader, load
from pm_ansible.cli import main, update_warnings


class redirect_stdany(object):
    '''
    Context for redirect any output to own stream.

    .. note::
        - On context return stream object.
        - On exit return old streams
    '''
    _streams = ["stdout", "stderr"]

    def __init__(self, new_stream=io.StringIO(), streams=None):
        '''
        :param new_stream: -- stream where redirects all
        :type new_stream: object
        :param streams: -- names of streams like ``['stdout', 'stderr']``
        :type streams: list
        '''
        self._streams = streams or self._streams
        self.stream = new_stream
        self._old_streams = {}

    def __enter__(self):
        for stream in self._streams:
            self._old_streams[stream] = getattr(sys, stream)
            setattr(sys, stream, self.stream)
        return self.stream

    def __exit__(self, exctype, excinst, exctb):
        for stream in self._streams:
            setattr(sys, stream, self._old_streams.pop(stream))


class CLITests(unittest.TestCase):
    def _get_reference(self, args=(), is_json=True):
        output = io.StringIO()
        with redirect_stdany(output, ['stdout']) as out:
            _argv = ['pm-ansible']
            if self.handler:
                _argv.append(self.handler)
            _argv += list(args)
            main(_argv)
            value = out.getvalue()
        return json.loads(value) if is_json else value

    def _get_ansible(self, args=()):
        return self._get_reference(args, is_json=False)

    def test_ansible_modules(self):
        self.handler = 'modules'
        result = self._get_reference()
        self.assertTrue(isinstance(result, list), result)

        module_name = 'commands.shell'
        for i in range(2):
            result = self._get_reference(['--get', module_name, '--detail'])
            self.assertEqual(result[0]['path'], module_name)
            data = load(result[0]['doc_data'], Loader=Loader)
            self.assertEqual(data['module'], module_name.split('.')[-1])
            self.assertIn(
                data['short_description'],
                ["Execute commands in nodes.", "Execute shell commands on targets"]
            )
        result = self._get_reference([
            '--get', 'polemarch.project.test_pack.test_packed_module',
            '--detail', '--path', './test_modules'
        ])
        data = load(result[0]['doc_data'], Loader=Loader)
        self.assertEqual(data['short_description'], "Test module")
        result = self._get_reference(['--path', './test_modules'])
        self.assertEqual(len(result), 2)
        self.assertIn('polemarch.project.test_pack.test_packed_module', result)
        self.assertIn('polemarch.project.test_module', result)

    def test_ansible_call(self):
        self.handler = None
        result = self._get_ansible(['ansible', 'all', '--list-hosts'])
        self.assertEqual('  hosts (0):', result.split('\n')[-2])

        with self.assertRaises(SystemExit) as err:
            self._get_ansible([
                'ansible-playbook', '--inventory', '127.0.0.0,', 'test.yml'
            ])
        self.assertEqual(err.exception.code, 4)
        result = self._get_ansible([
            'ansible',
            'all',
            '-m', 'setup',
            '-i', 'localhost,',
            '--connection', 'local'
        ])
        json.dumps(result)

    def test_cli_reference(self):
        self.handler = 'reference'
        results = self._get_reference()
        result = results['keywords']
        self.assertTrue(isinstance(result.get('module', None), dict))
        self.assertTrue(isinstance(result.get('playbook', None), dict))
        self.assertTrue(isinstance(result.get('galaxy', None), dict))

        results = self._get_reference(['module'])
        result = results['keywords']
        self.assertTrue(isinstance(result.get('module', None), dict))
        self.assertTrue(result.get('playbook', None) is None)
        self.assertTrue(result.get('galaxy', None) is None)

        results = self._get_reference(['module', 'playbook'])
        result = results['keywords']
        self.assertTrue(isinstance(result.get('module', None), dict))
        self.assertTrue(isinstance(result.get('playbook', None), dict))
        self.assertTrue(result.get('galaxy', None) is None)
        self.assertTrue('inventory' in result['playbook'])

        results = self._get_reference(['module', 'playbook', '--exclude', 'inventory'])
        result = results['keywords']
        self.assertTrue(isinstance(result.get('module', None), dict))
        self.assertTrue(isinstance(result.get('playbook', None), dict))
        self.assertTrue(result.get('galaxy', None) is None)
        self.assertTrue(not 'inventory' in result['playbook'])

    def test_parse_inventory(self):
        self.handler = 'inventory_parser'
        valid_hosts = {
            'test-host-1.com': dict(ansible_host='10.10.10.1'),
            'test-host-2.com': dict(ansible_host='10.10.10.2'),
            'test-host-3.com': dict(ansible_host='10.10.10.3'),
            'test-host-4.com': dict(ansible_host='10.10.10.4'),
            'test-host-5.com': dict(ansible_host='10.10.10.5'),
            'test-host-6.com': dict(ansible_host='10.10.10.6'),
            'test-host-7.com': dict(ansible_host='10.10.10.7'),
            'test-host-8.com': dict(ansible_host='10.10.10.8'),
        }
        valid_groups = {
            'test-group-1': {
                'hosts': ['test-host-1.com'],
                'groups': list(),
                'vars': dict(
                    ansible_user='ubuntu',
                    ansible_ssh_private_key_file='example-key'
                ),
            },
            'test-group-2': {
                'hosts': ['test-host-2.com'],
                'groups': list(),
                'vars': dict(),
            },
            'parent-group': {
                'hosts': list(),
                'groups': ['child-group-1', 'child-group-2'],
                'vars': dict(),
            },
            'child-group-1': {
                'hosts': ['test-host-3.com', 'test-host-4.com'],
                'groups': list(),
                'vars': dict(),
            },
            'child-group-2': {
                'hosts': ['test-host-4.com', 'test-host-5.com'],
                'groups': list(),
                'vars': dict(),
            },
        }
        valid_inv_vars = {
            'ansible_connection': 'ssh',
            'ansible_ssh_extra_args':'-o StrictHostKeyChecking=no',
        }
        result = self._get_reference(['test-inventory'])

        for record in result['hosts']:
            self.assertIn(record['name'], valid_hosts.keys())
            self.assertEqual(
                list(valid_hosts[record['name']].keys()),
                list(record['vars'].keys())
            )
            for key, value in valid_hosts[record['name']].items():
                self.assertEqual(record['vars'][key], value)

        for record in result['groups']:
            self.assertIn(record['name'], valid_groups.keys())
            self.assertEqual(
                list(valid_groups[record['name']]['hosts']),
                list(record['hosts'])
            )
            self.assertEqual(
                list(valid_groups[record['name']]['groups']),
                list(record['groups'])
            )
            self.assertEqual(
                sorted(list(valid_groups[record['name']]['vars'].keys())),
                sorted(list(record['vars'].keys()))
            )
            for key, value in valid_groups[record['name']]['vars'].items():
                self.assertEqual(record['vars'][key], value)

        self.assertEqual(
            sorted(list(valid_inv_vars.keys())),
            sorted(list(result['vars'].keys()))
        )
        for key, value in valid_inv_vars.items():
            self.assertEqual(result['vars'][key], value)

    def test_config(self):
        self.handler = 'config'
        result = self._get_reference()
        etalon_dir = os.getcwd()+'/roles'
        self.assertEqual(result.get('DEFAULT_ROLES_PATH', [None])[0], etalon_dir)

if __name__ == '__main__':
    unittest.main('tests')
